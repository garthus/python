inventaire = [
("pommes", 22),
("melons", 4),
("poires", 18),
("fraises", 76),
("prunes", 51),
]


# sorted


print(sorted(inventaire, key=lambda nombre:nombre[1], reverse=True ))


#---------------------------------


# list.sort

def nombre(x):
    return x[1]

inventaire.sort(key=nombre,reverse=True)
print(inventaire)
