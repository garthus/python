import random
import os
import sys
import pickle
from donnees import *


def enregistrement() :                                                                      # debut de partie enregistrement du user dans bdd
    global name , score_recupere
    name = input("\n Quel est ton Pseudo ? : ")
    if os.path.isfile("score") and os.path.getsize("score") > 0:
        with open("score","rb") as fichier :
            mon_depickler = pickle.Unpickler(fichier)
            score_recupere = mon_depickler.load()
    else :
        None

    with open("score","wb") as fichier :
        if name in score_recupere.keys() :
            None
        else :
            score_recupere[name] = 0
            mon_pickler = pickle.Pickler(fichier)
            mon_pickler.dump(score_recupere)

def choix_mot() :                                                                       # choix aleatoire du mot dans liste
    global mots, mot_choisi
    mot_choisi = random.choice(mots)
    return mot_choisi

def choix_lettre() :                                                                        # lettre rentrée par l'utilisateur
    global lettre_choisi
    lettre = input("\nchoisis une lettre : ")
    while len(lettre) > 1 or lettre.isalpha() == False :
        print("\n Il ne faut rentrer qu'une lettre !") 
        lettre = input("\nchoisis une lettre : ")
    else :
        print("\nVous avez choisit la lettre", lettre.lower())
        lettre_choisi = lettre.lower()
        return lettre_choisi

def mot_temp() :                                                                            # fontcion masquant le mot choisi avec des etoiles 
    global mot_choisi, mot_temp
    mot_temp= list(mot_choisi)
    i = 0
    while i < len(mot_temp) :
        mot_temp[i] = "*"
        i += 1
    #mot_temp = "".join(mot_temp)
    return mot_temp


def verification() :                                                                        # fonction verifiant si la lettre choisi est présente dans le mot choisi
    global mot_choisi, lettre_choisi, mot_temp , essai
    i = 0
    chance = 0
    while i < len(mot_choisi) :
        if mot_temp[i] == lettre_choisi :
           print("tu as deja trouvée cette lettre !\n")
        elif mot_choisi[i] == lettre_choisi :
           mot_temp[i] = lettre_choisi
           print(" Une lettre trouvée Bravo !\n", "".join(mot_temp))
        else :
           chance +=1
        i += 1
    if chance == len(mot_choisi) :
        print("Non la lettre n'est pas dans le mot !\n", "".join(mot_temp))
        essai += 1
        return essai

def perdu() :                                                                               # fonction apres 8 essais partie perdue
    global essai
    if essai == 8 :
        print("\n Désolé vous avez Perdu ! le mot etait : " , mot_choisi, "\n Ton score est Desormais nul : 0 \n" )
        with open("score","wb") as fichier :
            score_recupere[name] = 0
            mon_pickler = pickle.Pickler(fichier)
            mon_pickler.dump(score_recupere)
        sys.exit()
    else :
        None

def gagne() :                                                                               # fonction apres mot trouvé
    global mot_temp, score_recupere
    if "".join(mot_temp).isalpha() == True :
        print(" \n Bravo tu as trouvé le Mot !!!!")
        with open("score","wb") as fichier :
            score_recupere[name] += 8 - essai
            print("Ton score est de ",score_recupere[name],"\n")
            mon_pickler = pickle.Pickler(fichier)
            mon_pickler.dump(score_recupere)
        sys.exit()
    else :
        None

