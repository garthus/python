import math

deg = input ("combien de degres: ")
mi = input ("combien de minutes: ")
sec = input ("combien de secondes: ")

tot = (float(sec)/3600) + (float(mi)/60) + float(deg)     # conversion du tout en °

print ( "Cela Correspond à", int(tot)*math.pi/180 )       # conversion en radians
