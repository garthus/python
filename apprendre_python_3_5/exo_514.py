t1 = [ 32, 5, 12, 8, 3, 75, 2, 15 ]     #liste de depart

i = 0
pa = []                                 # liste des paires
im = []                                 # liste des impaires

while i < len(t1) :                     
    if t1[i] % 2 == 0 :                 # si le nombre est un multiple de 2
        pa.append(t1[i])                # l'ajouter a la liste paire
    else :
        im.append(t1[i])                # sinon l'ajouter a la liste impaire
    i = i + 1
    
print("Voici les numeros paires de la liste", pa)
print("Voici les numeros impaires de la liste", im)
