from tkinter import *
import math

# procédure générale de déplacement du point Rouge :
def avanceR(gd, hb):
    global x1, y1, d
    x1, y1 = x1 +gd, y1 +hb
    can1.coords(oval1, x1,y1, x1+30,y1+30)
    
# gestionnaires d'événements :
def depl_gaucheR():
    avanceR(-10, 0)
    valeur.set(d)
def depl_droiteR():
    avanceR(10, 0)
    valeur.set(d)
def depl_hautR():
    avanceR(0, -10)
    valeur.set(d)
def depl_basR():
    avanceR(0, 10)
    valeur.set(d)

# procédure générale de déplacement du point bleu :
def avanceB(gd,hb):
    global x2,y2 
    x2, y2 = x2 + gd, y2 + hb
    can1.coords(oval2,x2,y2, x2+10, y2+10)


# gestionnaires d'événements :
def depl_gaucheB():
    avanceB(-10, 0)
def depl_droiteB():
    avanceB(10, 0)
def depl_hautB():
    avanceB(0, -10)
def depl_basB():
    avanceB(0, 10)

#------ Programme principal -------
# les variables suivantes seront utilisées de manière globale :
x1, y1 = 10, 10
x2,y2  = 50, 50
d=int(math.sqrt((x1-x2)**2+(y1-y2)**2))
# coordonnées initiales

# Création du widget principal ("maître") :
fen1 = Tk()
fen1.title("Exercice d'animation avec tkinter")
# création des widgets "esclaves" :
can1 = Canvas(fen1,bg='dark grey',height=300,width=300)
oval1 = can1.create_oval(x1,y1,x1+30,y1+30,width=2,fill='red')
oval2 = can1.create_oval(x2,y2,x2+10,y2+10,width=2,fill='blue')
can1.pack(side=LEFT)
Button(fen1,text='Quitter',command=fen1.quit).pack(side=BOTTOM)
Button(fen1,text='Pt Rouge Gauche',command=depl_gaucheR).pack()
Button(fen1,text='Pt Rouge Droit',command=depl_droiteR).pack()
Button(fen1,text='Pt Rouge Haut',command=depl_hautR).pack()
Button(fen1,text='Pt Rouge Bas',command=depl_basR).pack()
Button(fen1,text='Pt Bleu Gauche',command=depl_gaucheB).pack()
Button(fen1,text='Pt Bleu Droit',command=depl_droiteB).pack()
Button(fen1,text='Pt Bleu Haut',command=depl_hautB).pack()
Button(fen1,text='Pt bleu Bas',command=depl_basB).pack()
Label(fen1,textvariable=valeur).pack()

# démarrage du réceptionnaire d’évènements (boucle principale) :
fen1.mainloop()
