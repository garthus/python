l1 = input("entrez la premiere longueur du triangle: ")
l2 = input("entrez la deuxieme longueur du triangle: ")
l3 = input("entrez la troisieme longueur du triangle: ")
print("\n")

ll = 0
c1 = float(l1)
c2 = float(l2)
c3 = float(l3)

if c2 < c1 > c3 :                                           # determination du coté le plus grand ll
    ll = c1
    la = c2
    lb = c3
elif c1 < c2 > c3:
    ll = c2
    la = c1
    lb = c3
else :
    ll = c3
    la = c2
    lb = c1



if c1 == c2 == c3:                                          # les trois cotés sont égaux
    print("Le triangle est equilateral")
elif c1 == c2 or c2 == c3 or c3 == c1 :                     # si deux cotés de meme longueur
    if ll**2 == la**2 + lb**2 :
        print("le triangle est isocele et rectangle")       # et que pythagore se verifie  le triangle est isocele rectangle
    else :
        print("le triangle est isocele")                    # sinon il est que isocele
elif ll**2 == la**2 + lb**2 :                                 # si pythagore se verifie c'est un triangle rectangle
    print("le triangle est rectangle")
else :
    print("c'est un triangle quelconque")
