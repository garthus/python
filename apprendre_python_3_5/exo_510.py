m = input("Rentrez un mot: ")            # mot a rentrer
i=len(m)-1                               # longueur total -1 caractere (commence a 0 ...)
ch = ""                                  # nouvelle chaine vide

while i >= 0 :                           # tant que la longueur totale n'atteint pas 0
    ch = ch + m[i]                       # chaine = caractere precedent + caractere correspondant au numero de caractere
    i = i - 1                            # decompte pour partir de la fin vers le debut du mot


print(ch)


if ch == m :
    print("c'est un palindrome")
