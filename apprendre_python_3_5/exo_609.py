a = input("rentrez une année: ")

if int(a) % 4 == 0 :                            # si l'année est un multiple de 4
    
    if int(a) % 100 != 0 :                      # et pas un multiple de 100
        print("l année est bissextile")
    
    elif int(a) % 100 == 0 :                    # ou un multiple de 100  mais divisible par 400  : l'année est bissextile. Sinon non.
        
        if int(a) % 400 == 0 :
            print("l'année est bissextile")
        else :
            print("lannée n'est pas bissextile")
else :
    print("l'année n'est pas bissextile")
