ne = input( "entrez un nombre de secondes : ")     # Nombre entré en Secondes
a= float(ne)/31536000                              # conversion des secondes en années
mo= (float(ne)%31536000)/2628000                   # conversion du restant des années en mois
j= (float(ne)%2628000)/86400                       # conversion du restant des mois en jours
h= (float(ne)%86400)/3600                          # conversion du restant des jours en heures
mi= (float(ne)%3600)/60                            # conversion du restant des heures en minutes
s=(float(ne)%60)                                   # conversion du restant des minutes en secondes

print("cela correspond a", int(a) , "années" , int(mo) , "mois" , int(j) , "jours" , int(h) , "heures", int(mi) , "minutes" , int(s) , "secondes")
