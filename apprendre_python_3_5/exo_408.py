m=1        # nombre multiplicateur
n=13       # nombre a multiplier

while m < 51 :            # on veut les 50 premiers multiple de 13
    if (n*m)%7 == 0 :     # si ces multiples sont des multiples de 7
        print (n*m)       # afficher le resultat de la multiplication a l'ecran
    m=m+1
