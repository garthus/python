def inverse(ch) :

    i=0
    x=len(ch)-1
    m=""
    while i < len(ch) :          # tant que i est inferieur a la longueur du mot
        m += ch[x]               # rajouter le caractere correspondant a l'index de x (derniere lettre du mot)
        i = i + 1
        x = x - 1                # decrementer x pour parcourir tout le mot
    print(m)                     # afficher le mot a l'envers

inverse("caractere")
