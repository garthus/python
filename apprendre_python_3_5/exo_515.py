t1 = ['Jean', 'Maximilien', 'Brigitte', 'Sonia', 'Jean-Pierre', 'Sandra']

i = 0
s = []
m = []

while i < len(t1) :             # sur tous les mots de la liste
    if len(t1[i]) >= 6 :        # si la longueur du mot contient + ou = a 6 caracteres ajouter a la liste "s"
        s.append(t1[i])
    else :
        m.append(t1[i])         # sinon ajouter a la liste "m"
    i = i + 1

print("voici les mots de plus de six lettres",s)
print("voici les mots de moins de six lettres",m)