# Détection et positionnement d'un clic de souris dans une fenêtre :

from tkinter import *

def pointeur(event):
    chaine.configure(text = "Clic détecté en X =" + str(event.x) +\
                            ", Y =" + str(event.y))
    x0=int(str(event.x))
    y0=int(str(event.y))

    cadre.create_oval(x0-5,y0-5,x0+5,y0+5,width=2,fill='red')

fen=Tk()
cadre=Canvas(fen, width =200, height =150, bg="light yellow")
cadre.bind("<Button-1>", pointeur)
cadre.pack()
chaine=Label(fen)
chaine.pack()

fen.mainloop()
