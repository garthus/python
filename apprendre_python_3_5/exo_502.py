import math

rad = input ("Donner un nombre en Radians: ")
degres = 180 / math.pi * float(rad)
minutes = (float(degres)- int(degres))*60
secondes = (float(minutes)- int(minutes))*60

print("Cela Correspond à", int(degres), "degres", int(minutes), "minutes", int(secondes), "secondes")