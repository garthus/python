from tkinter import *
from random import randrange

def damier():
    x0,y0,x1,y1=0,0,50,50                                        # coordonnées du premier Carré
    height=500                                                   # longueur du damier
    while y0 < height :                                         # tant que y ne depasse la longueur du damier
        while x1 < height :                                         # tant que x1 ne depasse pas la largeur du damier
            can.create_rectangle(x0,y0,x1,y1,fill='blue')           # creer un rectangle en partant du depart
            can.create_rectangle(x0+100,y0,x1+100,y1,fill='blue')   # recommencer l'operation en laissant un blanc
            can.create_rectangle(x0+50,y0+50,x1+50,y1+50,fill='blue') # sur les blancs creer un carré en dessous
            x1+=100 ; x0+=100
        x0,y0,x1,y1=0,y0+100,50,y1+100                              # on avance de deux lignes (deja ecrites grace a l'operation precedente et rebelotte
    
def pions():
    n=[0,50,100,150,200,250,300,350,400,450]                        # choix d'un coin gauche d'un carré du canvas
    c1=randrange(10)                                                    #nombre au hasard dans la liste
    c2=randrange(10)                                                    
    nx=n[c1]                                                        # nombre au hasard point de depart de x0
    ny=n[c2]                                                        # nombre au hasard point de depart de y0
    can.create_oval(nx,ny,nx+50,ny+50,width=2,fill='red',tag=10)    # creation du pion dans la case. Pas unique ??!!

fen=Tk()
can= Canvas(fen,width=500,height=500,bg='white')
can.pack(side=TOP,padx=3,pady=3)
b1= Button(fen,text="Damier",command=damier)
b1.pack(side=LEFT,padx=3,pady=3)
b2= Button(fen,text="Pion",command=pions)
b2.pack(side=RIGHT,padx=10,pady=10)
fen.mainloop()
