def changeCar(ch,ca1,ca2,debut,fin):
    i=0
    new=''

    while i < debut :                               # tant que i n'est pas egal a debut ( commencement du range  a changer)
        new += ch[i]                                # laisser la phrase telle quelle
        i = i + 1 
    while debut != fin :                            # tant que le range voulu  n'est pas parcouru
        if ch[debut] == ca1 :                       # si le caractere du debut est égal au caractere voulant etre changé
            new += ca2                              # ajouter le nouveau caractere a la phrase transformée
        else :                                      
            new += ch[debut]
        debut = debut + 1
    while fin < len(ch) :                           # a la fin du range tant que la phrase n'est pas fini
        new += ch[fin]                              # ajouter les caracteres de la phrase initiale a la phrase transformée
        fin = fin + 1
    print(new)                                      # Afficher le mot transformé

changeCar("ou est l'orange du pilote","o","b",0,12)
