from turtle import *

def carre(taille, couleur, angle) :
    "fonction qui dessine un carré de taille et de couleur déterminées"
    setheading(angle)
    color(couleur)
    c =0
    while c < 4 :
        forward(taille)
        right(90)
        c=c+1

def triangle(taille, couleur, angle) :
    setheading(angle)
    color(couleur)
    c=0
    while c < 3 :
        forward(taille)
        right(120)
        c=c+1

def etoile(taille,couleur, angle) :
    setheading(angle)
    color(couleur)
    c=0
    while c < 5 :
        forward(taille)
        right(144)
        c=c+1

