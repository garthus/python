p = input("Entrez un mot: ")  # mot a sasir
n = 0                         # nombre de lettre dans le mot
e = 0                         # nombre de E dans le mot

while n < len(p):                            # tant que le nombre de lettre est inferieur a la longueur du mot

    if (p[n]) == "e" :                       # et si la lettre est un e incrémenté la valeur e de 1
       e = e + 1

    n = n + 1
    
print ("il y a", e , "E dans ce mot")
