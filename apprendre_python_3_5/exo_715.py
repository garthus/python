def volboite(x1="",x2="",x3="") :

    if x1 == x2 == x3 == "" :                   # si il n'y  pas d'argument
        return "-1"                             # retourner une erreur
    elif x1 !="" and x2 == x3 == "" :           # si il n'y a qu'un argument
        return (x1**3)                          # retourner volume d'un cube
    elif x1 !="" and x2 !=0 and x3 == "" :      # si il y a deux arguments renvoyer le volume d'un prisme 
        return (x1**2*x2)                       
    elif x1 and x2 and x3 !="" :                # si il y en a trois renvoyer le volume d'un parallelepipede
        return (x1*x2*x3)

print(volboite(5.2))
