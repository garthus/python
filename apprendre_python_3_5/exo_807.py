# Petit exercice utilisant la bibliothèque graphique tkinter
from tkinter import *
from random import randrange
# --- définition des fonctions gestionnaires d'événements : ---
def anneau_haut():
    "Tracé d'une ligne dans le canevas can1"
    global x1, y1, x2, y2, col, i
    can1.create_oval(x1,y1,x2,y2,width=2,fill=couleur[i]) 
    # modification des coordonnées pour creer un deuxieme rond blanc dans le rond pour pour faire un anneau 
    x1,y1,x2,y2 = x1+10,y1-10,x2-10,y2+10
    can1.create_oval(x1,y1,x2,y2,width=2,fill='white')
    # retour a la taille de rond initiale
    x1,y1,x2,y2 = x1-10,y1+10,x2+10,y2-10
    #incrementation de i pour changement de couleur
    i=i+1

def anneau_bas():
    global x1, y1, x2, y2, col, i
    # deplacement lateral
    x1,y1,x2,y2 = x1+50,y1+50,x2+50,y2+50
    can1.create_oval(x1,y1,x2,y2,width=2,fill=couleur[i])
    # modification des coordonnées pour creer un deuxieme rond blanc dans le rond pour pour faire un anneau
    x1,y1,x2,y2 = x1+10,y1-10,x2-10,y2+10
    can1.create_oval(x1,y1,x2,y2,width=2,fill='white')
    # retour a la taille de rond initiale,deplacement lateral et incrementation de i
    x1,y1,x2,y2 = x1-10,y1+10,x2+10,y2-10
    x1,y1,x2,y2 = x1+50,y1-50,x2+50,y2-50
    i=i+1
i=0
couleur=["blue","yellow","black","green","red"]

#------ Programme principal -------
# les variables suivantes seront utilisées de manière globale :
x1, y1, x2, y2 = 10, 100, 100, 10 # coordonnées de la ligne
# Création du widget principal ("maître") :
fen1 = Tk()
# création des widgets "esclaves" :
can1 = Canvas(fen1,bg='white',height=200,width=500)
can1.pack(side=LEFT)
bou1 = Button(fen1,text='Quitter',command=fen1.quit)
bou1.pack(side=BOTTOM)
bou2 = Button(fen1,text='Tracer le Premier Anneau',command=anneau_haut)
bou2.pack()
bou3 = Button(fen1,text="Tracer le Deuxieme Anneau",command=anneau_bas)
bou3.pack()
bou4 = Button(fen1,text='Tracer le troisieme Anneau',command=anneau_haut)
bou4.pack()
bou5 = Button(fen1,text="Tracer le Quatrieme Anneau",command=anneau_bas)
bou5.pack()
bou6 = Button(fen1,text='Tracer le Cinquieme Anneau',command=anneau_haut)
bou6.pack()
fen1.mainloop() # démarrage du réceptionnaire d’événements
fen1.destroy() # destruction (fermeture) de la fenêtre

