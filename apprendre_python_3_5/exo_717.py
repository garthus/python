def eleMax(serie,debut=0,fin=-1):

    nbmax=0

    if fin == -1 :                                              # si la valeur de fin n'est pas defini toute la liste est balayée
        fin += len(serie) 
    while debut < fin :
        if serie[debut] > serie[debut+1] > nbmax :              # si le nombre initial est plus grand que celui a sa droite direct et que le nbmax
            nbmax =  serie[debut]                               # alors il vaut le nbmax
        elif nbmax < serie[debut+1] :                           # si le nbmax est inferieur au nombre a la droite direct du nombre initial
            nbmax =  serie[debut+1]                             # alors il vaut le nbmax
        else:
            None                                                
        debut = debut + 1                                       # on incremente pour parcourir la liste
    print(nbmax)

serie = [12,40,79,43,84,2,20,105,14,56,330,67]
eleMax(serie,0,3)
