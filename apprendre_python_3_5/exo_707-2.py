from dessins_tortue import *
up() # relever le crayon
goto(-150, 50) # reculer en haut à gauche
# dessiner dix carrés rouges, alignés :
i = 0
x = 20
if i < 4 : 
    down() # abaisser le crayon
    etoile(x,'red',40) # tracer un carré
    up() # relever le crayon
    forward(50) # avancer + loin
    i = i + 1
    x = x + 10

elif 4 <= i <= 8 :
    down() # abaisser le crayon
    etoile(x,'red',40) # tracer un carré
    up() # relever le crayon
    forward(50) # avancer + loin
    i = i + 1
    x = x - 10
